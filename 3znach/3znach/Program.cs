﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3znach
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Тест 3-х значного числа");
            Console.WriteLine("");

            int z, y, c;
            int summa, proiz;
            string povtor;

            try
            {

                do
                {

                    Console.Write("Введите 3-х значное число: ");
                    int x = int.Parse(Console.ReadLine());

                    Console.Write("Введите число для теста: ");
                    int a = int.Parse(Console.ReadLine());

                    Console.WriteLine("");


                    z = x / 100;
                    y = x / 10 % 10;
                    c = x % 10;
                    summa = z + y + c;
                    proiz = z * y * c;
                    Console.WriteLine($"Сумма чисел: {summa}");
                    Console.WriteLine($"Произведение чисел: {proiz}");

                    if (a > z * y * c) Console.Write("Тестовое число больше произведения");

                    if ((z + y + c) % 5 == 0) Console.WriteLine("Сумма кратна 5");

                    if (a % (z + y + c) == 0) Console.WriteLine("Тестовое число кратно сумме");



                    Console.WriteLine("");
                    Console.Write("Введите 'Y', если хотите повторить: ");
                    povtor = Console.ReadLine();
                }
                while (povtor == "Y");

            }
            catch
            {
                Console.WriteLine("Данные неверны. Программа будет закрыта");
            }
        }
    }
}
