﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arifmetika
{
    class Program
    {
        static void Main(string[] args)
        {
            int A, B, C;
            string povtor;

            Console.WriteLine("Арифметика");

            try
            {

                do
                {

                    Console.Write("Укажите первый множитель:");
                    A = Convert.ToInt32(Console.ReadLine());

                    Console.Write("Укажите второй множитель:");
                    B = Convert.ToInt32(Console.ReadLine());

                    Console.Write("Укажите предполагаемый ответ:");
                    C = Convert.ToInt32(Console.ReadLine());

                    if (C == A * B)
                    {
                        Console.WriteLine("Правильный ответ");
                    }
                    else
                    {
                        Console.WriteLine("Неправильный ответ");
                    }


                    Console.WriteLine("Введите 'Y', если хотите повторить");
                    povtor = Console.ReadLine();

                }
                while (povtor == "Y");

            }
            catch
            {
                Console.WriteLine("Данные неверны. Программа будет закрыта");
            }
            Console.ReadKey();

        }
    }
}
