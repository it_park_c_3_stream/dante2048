﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassString
{
    class GrossString
    {
        char[] symbols;

        private char[] smallAlfavit = new char[] { 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я' };

        private char[] bigAlfavit = new char[] { 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я' };

        #region Конструктор строки
        public GrossString()
        {
            symbols = new char[] { };
        }

        public GrossString(string line)
        {
            try
            {
                symbols = new char[line.Length];
                for (int i = 0; i < line.Length; i++)
                {
                    symbols[i] = char.Parse(line[i].ToString());
                }
            }
            catch
            {
                symbols = new char[] { };
            }
        }

        public GrossString(GrossString inGrossString)
        {
            symbols = new char[inGrossString.symbols.Length];
            Array.Copy(inGrossString.symbols, symbols, inGrossString.symbols.Length);
        }

        #endregion

        #region Вспомогательные методы

        private void AddSymbol(char inSymbol)
        {
            Array.Resize(ref symbols, symbols.Length + 1);
            symbols[symbols.Length - 1] = inSymbol;
        }

        public void AddSymbols(GrossString inGrossString)
        {
            for (int i = 0; i < inGrossString.symbols.Length; i++)
            {
                this.AddSymbol(inGrossString.symbols[i]);
            }
        }

        public int CompareTo(GrossString inGrossString)
        {

            if (symbols.Length > inGrossString.symbols.Length)
            {
                return 1;
            }
            else if (symbols.Length < inGrossString.symbols.Length)
            {
                return -1;
            }


            return 0;
        }

        public GrossString ReversionString()
        {
            GrossString tempSymbols = new GrossString(this);
            for (int i = 0, j = tempSymbols.symbols.Length - 1; i < symbols.Length && j >= 0; i++, j--)
            {
                symbols[i] = tempSymbols.symbols[j];
            }

            return this;
        }

        public GrossString SortingAlfavit()
        {
            GrossString tempSymbols = new GrossString(this);

            Array.Resize(ref symbols, 0);

            for (int i = 0; i < 32; i++)
            {
                for (int j = 0; j < tempSymbols.symbols.Length; j++)
                {
                    if (bigAlfavit[i] == tempSymbols.symbols[j])
                    {
                        Array.Resize(ref symbols, symbols.Length + 1);
                        symbols[symbols.Length - 1] = bigAlfavit[i];

                    }
                }
                for (int j = 0; j < tempSymbols.symbols.Length; j++)
                {
                    if (smallAlfavit[i] == tempSymbols.symbols[j])
                    {
                        Array.Resize(ref symbols, symbols.Length + 1);
                        symbols[symbols.Length - 1] = smallAlfavit[i];
                    }
                }
            }


            return this;

        }

        public void Substring(GrossString inGrossString, int numberBegin, int numberEnd)
        {
            int indexBegin = numberBegin - 1;
            int indexEnd = numberEnd - 1;

            if (indexBegin >= 0 && indexEnd <= inGrossString.symbols.Length - 1)
            {
                Array.Resize(ref symbols, 0);

                for (int i = indexBegin; i <= indexEnd; i++)
                {
                    Array.Resize(ref symbols, symbols.Length + 1);
                    symbols[symbols.Length - 1] = inGrossString.symbols[i];
                }
            }
            else
            {
                Console.WriteLine("Заданный диапозон не соответствует длине строки");
            }


        }

        public GrossString Replacement(char searchSymbol, char exchangeSymbol = '`')
        {
            GrossString tempSymbols = new GrossString(this);

            Array.Resize(ref symbols, 0);


            for (int i = 0; i < tempSymbols.symbols.Length; i++)
            {
                if (tempSymbols.symbols[i] == searchSymbol && exchangeSymbol != '`')
                {
                    Array.Resize(ref symbols, symbols.Length + 1);
                    symbols[symbols.Length - 1] = exchangeSymbol;
                }
                else if (tempSymbols.symbols[i] != searchSymbol && exchangeSymbol == '`')
                {
                    Array.Resize(ref symbols, symbols.Length + 1);
                    symbols[symbols.Length - 1] = tempSymbols.symbols[i];
                }

            }

            return this;

        }


        #endregion

        #region Перегрузки
        public override string ToString()
        {
            string output = string.Empty;
            for (int i = 0; i < symbols.Length; i++)
            {
                output += symbols[i];
            }

            return output;
        }
        public static bool operator >(GrossString GrossSt1, GrossString GrossSt2)
        {
            return GrossSt1.CompareTo(GrossSt2) == 1;
        }
        public static bool operator <(GrossString GrossSt1, GrossString GrossSt2)
        {
            return GrossSt1.CompareTo(GrossSt2) == -1;
        }
        public static bool operator >=(GrossString GrossSt1, GrossString GrossSt2)
        {
            return GrossSt1.CompareTo(GrossSt2) == 1 || GrossSt1.CompareTo(GrossSt2) == 0;
        }
        public static bool operator <=(GrossString GrossSt1, GrossString GrossSt2)
        {
            return GrossSt1.CompareTo(GrossSt2) == -1 || GrossSt1.CompareTo(GrossSt2) == 0;
        }
        public static bool operator ==(GrossString GrossSt1, GrossString GrossSt2)
        {
            return GrossSt1.CompareTo(GrossSt2) == 0;
        }
        public static bool operator !=(GrossString GrossSt1, GrossString GrossSt2)
        {
            return GrossSt1.CompareTo(GrossSt2) != 0;
        }
        public static GrossString operator +(GrossString GrossSt1, GrossString GrossSt2)
        {
            GrossString temp = new GrossString();
            GrossSt1.AddSymbols(GrossSt2);
            return temp;
        }



        #endregion

    }
}
