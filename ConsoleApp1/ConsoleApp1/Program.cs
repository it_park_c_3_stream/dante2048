﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            for (int i = 1; i < 11; i++)
            {
                for (int j = 1; j < 11; j++)
                {
                    if (((i * j)/10)==0)
                    {
                       Console.Write($"   {i * j}");
                    }
                    else
                    { 
                    if (((i * j) / 100) == 0)
                    {
                        Console.Write($"  {i * j}");
                    }
                    else
                    { 
                    if (((i * j) / 1000) == 0)
                    {
                        Console.Write($" {i * j}");
                    }
                    }
                    }
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
