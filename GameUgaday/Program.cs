﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            int otvet;
            string povtor;

            Console.WriteLine("Игра Угадайка");

            try
            {

                do
                {
                    Console.Write("Загадайте первое число:");
                    int pervoe = Convert.ToInt32(Console.ReadLine());

                    otvet = (pervoe * 100) + 90 + 9 - pervoe;

                    Console.WriteLine($"Ответ:{otvet}");

                    Console.WriteLine("Введите 'Y', если хотите повторить");
                    povtor = Console.ReadLine();
                }
                while (povtor == "Y");

            }
            catch
            {
                Console.WriteLine("Данные неверны. Программа будет закрыта");
            }
            Console.ReadKey();


        }
    }
}
