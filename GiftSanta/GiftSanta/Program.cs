﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiftSanta
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0, i = 0;

            Console.Write("Введите вес ириски:");
            int iris = int.Parse(Console.ReadLine());

            Console.Write("Введите вес мандаринки:");
            int mand = int.Parse(Console.ReadLine());

            Console.Write("Введите вес пряника:");
            int Pryanik = int.Parse(Console.ReadLine());

            Console.Write("Введите общий вес подарка:");
            int W = int.Parse(Console.ReadLine());



            for (int maxIris = 0; maxIris <= W; maxIris += iris)
            {
                
                for (int maxMand = 0; maxMand <= W; maxMand += mand)
                {

                    for (int maxPryanik = 0; maxPryanik <= W; maxPryanik += Pryanik)
                    {
                        sum = maxIris + maxMand + maxPryanik;

                        if (sum == W)
                        {
                            i++;
                        }

                    }

                }

            }

            Console.WriteLine($"Вариантов: {i}");
            Console.ReadKey();

        }
    }
}
