﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Korni
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Корни квадратного уровнения");
            Console.WriteLine("");

            int a, b, c;
        
            string povtor;

            try
            {

                do
                {

                    
                    Console.WriteLine("Введите a");
                    a = int.Parse(Console.ReadLine());
                    Console.WriteLine("Введите b");
                    b = int.Parse(Console.ReadLine());
                    Console.WriteLine("Введите c");
                    c = int.Parse(Console.ReadLine());

                    double F = b * b - 4 * a * c;

                    if (F >= 0)
                    {
                        double x1 = (-b + Math.Sqrt(F)) / (2 * a);
                        double x2 = (-b - Math.Sqrt(F)) / (2 * a);
                        if ((Convert.ToInt32(x1) != x1) || (Convert.ToInt32(x2) != x2))
                        {
                            Console.WriteLine("Уравнение имеет вещественные корни");
                        }
                        else
                        {
                            Console.WriteLine(" Уравнение не имеет вещественных корней");
                        }
                    }
                    

                    Console.WriteLine("");
                    Console.Write("Введите 'Y', если хотите повторить: ");
                    povtor = Console.ReadLine();
                }
                while (povtor == "Y");

            }
            catch
            {
                Console.WriteLine("Данные неверны. Программа будет закрыта");
            }
        }
    }
}
