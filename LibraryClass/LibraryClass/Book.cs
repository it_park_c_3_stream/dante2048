﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace LibraryClass
{ 

    class Book
    {
        public string name;
        public string author;
        public int publicationData;
        public double rating;
        public char location;


        public Book(string inName, string inAuthor, int inPublicationData, double inRating, char inLocation)
        {
            name = inName;
            author = inAuthor;
            publicationData = inPublicationData;
            rating = inRating;
            location = inLocation;
        }

        public void PrintBook(int number)
        {
            Console.WriteLine("{0,-4}{1,-10}{2,-7}{3,-12}{4,-8}{5,-10}", number, name, author, publicationData, rating, location);
        }

        
        public void PrintBookToFile(int number, StreamWriter writer)
        {
            writer.WriteLine("{0,-4}{1,-10}{2,-7}{3,-12}{4,-8}{5,-10}", number, name, author, publicationData, rating, location);
        }

    }
}