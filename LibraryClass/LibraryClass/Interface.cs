﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryClass
{

    class Interface
    {

        public void ShowHeader()
        {
            Console.WriteLine("Список книг в библиотеке");
            Console.WriteLine(new string('-', 40));
            Console.WriteLine();
        }

        public void ShowMenu()
        {
            Console.WriteLine("Меню:");
            Console.WriteLine("1.Добавить новую книгу в библиотеку");
            Console.WriteLine("2.Удалить книгу из библиотеки");
            Console.WriteLine("3.Редактировать все параметры книги");
            Console.WriteLine("4.Сохранить каталог в файл");
            Console.WriteLine("5.Экспорт каталога для печати");
            Console.WriteLine("6.Загрузить каталог из файла");

            Console.WriteLine("0.Выход из программы");
        }

        public void ChoseMenuItem()
        {
            bool result;
            int item;

            do
            {
                Console.Write("Введите номер пункта меню: ");
                result = int.TryParse(Console.ReadLine(), out item);

                if (result == false || item < 0 || item > 7)
                {
                    Console.WriteLine("Неверный ввод, повторите попытку");
                }

            } while (result == false || item < 0 || item > 7);

            
        }

    }
}
