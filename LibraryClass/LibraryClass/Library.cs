﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace LibraryClass
{

    class Library
    {
        public Book[] library;

        public Library()
        {
            library = new Book[] { };
        }


        public void InputNewBook()
        {
            int item = library.Length - 1;
            bool result = true;

            do
            {
                Console.WriteLine("Введите название книги");
                library[item].name = Console.ReadLine();

                //for (int i = 0; i < library.Length; i++)
                //{
                //    if (name == library[i].name)
                //    {
                //        Console.WriteLine("Книга с таким названием уже есть");
                //        result = false;
                //        break;
                //    }
                //}


            } while (library[item].name.Length < 2 /*|| result == false*/);


            do
            {
                Console.WriteLine("Введите автора");
                library[item].author = Console.ReadLine();

            } while (library[item].author.Length < 2);

            do
            {
                Console.WriteLine("Введите год пибликации книги");
                result = int.TryParse(Console.ReadLine(), out library[item].publicationData);

            } while (result == false || library[item].publicationData <= 0);

            do
            {
                Console.WriteLine("Введите рейтинг книги");
                result = double.TryParse(Console.ReadLine(), out library[item].rating);

            } while (result == false || library[item].rating < 0);

            library[item].location = 'Б';

        }

        public void PrintLibrary()
        {
            Console.WriteLine("{0,-4}{1,-10}{2,-7}{3,-12}{4,-8}{5,-10}", "#", "Name", "Author", "Publication", "Rating", "Location");

            for (int i = 0; i < library.Length; i++)
            {
                library[i].PrintBook(i + 1);
            }

            Console.WriteLine();
            Console.WriteLine(new string('-', 40));
            Console.WriteLine();
        }

        public void AddBookToLibrary()
        {
            Array.Resize(ref library, library.Length + 1);
            library[library.Length - 1].InputNewBook();
        }

        public void DeleteBookFromLibraryByNumber(int number)
        {
            int index = number - 1;

            for (int i = index; i < library.Length - 1; i++)
            {
                library[i] = library[i + 1];
            }

            Array.Resize(ref library, library.Length - 1);
        }

        public void UpdateBookFromLibraryByNumber(int number)
        {
            int index = number - 1;
            library[index].InputNewBook();
        }


        #region дополительые методы работы с библиотекой

        public void SaveLibraryToFile(string fileName)
        {
            StreamWriter writer = new StreamWriter(fileName);
            writer.WriteLine(library.Length);

            for (int i = 0; i < library.Length; i++)
            {
                writer.WriteLine(library[i].name);
                writer.WriteLine(library[i].author);
                writer.WriteLine(library[i].publicationData);
                writer.WriteLine(library[i].rating);
                writer.WriteLine(library[i].location);
            }
            writer.Close();
        }


        public void ExportLibraryToFile(string fileName)
        {
            StreamWriter writer = new StreamWriter(fileName);

            writer.WriteLine("Список книг в библиотеке");
            writer.WriteLine(new string('-', 40));
            writer.WriteLine();

            writer.WriteLine("{0,-4}{1,-10}{2,-7}{3,-12}{4,-8}{5,-10}", "#", "Name", "Author", "Publication", "Rating", "Location");

            for (int i = 0; i < library.Length; i++)
            {
                library[i].PrintBookToFile(i + 1, writer);
            }

            writer.WriteLine();
            writer.WriteLine(new string('-', 40));
            writer.WriteLine();

            writer.Close();
        }

        public void LoadLibraryFromFile(string fileName)
        {
            StreamReader reader = new StreamReader(fileName);
            int length = int.Parse(reader.ReadLine());

            Book[] library = new Book[length];

            for (int i = 0; i < library.Length; i++)
            {
                library[i].name = reader.ReadLine();
                library[i].author = reader.ReadLine();
                library[i].publicationData = int.Parse(reader.ReadLine());
                library[i].rating = double.Parse(reader.ReadLine());
                library[i].location = char.Parse(reader.ReadLine());
            }
            reader.Close();

        }

        #endregion
    }
}
