﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace LibraryClass
{
    class Program
    {
         static void ShowHeader()
    {
        Console.WriteLine("Список книг в библиотеке");
        Console.WriteLine(new string('-', 40));
        Console.WriteLine();
    }

    static void ShowMenu()
    {
        Console.WriteLine("Меню:");
        Console.WriteLine("1.Добавить новую книгу в библиотеку");
        Console.WriteLine("2.Удалить книгу из библиотеки");
        Console.WriteLine("3.Редактировать все параметры книги");
        Console.WriteLine("4.Сохранить каталог в файл");
        Console.WriteLine("5.Экспорт каталога для печати");
        Console.WriteLine("6.Загрузить каталог из файла");

        Console.WriteLine("0.Выход из программы");
    }

        static int ChoseMenuItem()
    {
        bool result;
        int item;

        do
        {
            Console.Write("Введите номер пункта меню: ");
            result = int.TryParse(Console.ReadLine(), out item);

            if (result == false || item < 0 || item > 7)
            {
                Console.WriteLine("Неверный ввод, повторите попытку");
            }

        } while (result == false || item < 0 || item > 7);

            return item;
    }
        static void Main(string[] args)
        {
            Library library = new Library();
            int numberBook;
            bool result = true;
            string fileName;
            int choose;


            while (true)
            {
                Console.Clear();

                ShowHeader();

                library.PrintLibrary();

                ShowMenu();

                choose = ChoseMenuItem();

                switch (choose)
                {
                    case 1:

                        library.AddBookToLibrary();
                                break;
                    case 2:
                        #region Удаление
                        if (library.library.Length > 0)
                        {



                                    do
                                    {
                                        Console.Write("Введите номер для удаления: ");
                                        result = int.TryParse(Console.ReadLine(), out numberBook);
                                    } while (result == false || numberBook < 1 || numberBook > library.library.Length);
                            library.DeleteBookFromLibraryByNumber(numberBook);

                                    break;
                                

                        }
                        else
                        {
                            Console.WriteLine("Библиотека пуста");
                            Console.ReadKey();
                        }



                        break;
                    #endregion
                    case 3:
                        #region Изменение

                        if (library.library.Length > 0)
                        {

                            do
                            {
                                Console.Write("Введите номер для изменения: ");
                                result = int.TryParse(Console.ReadLine(), out numberBook);
                            } while (result == false || numberBook < 1 || numberBook > library.library.Length);

                            library.UpdateBookFromLibraryByNumber(numberBook);

                            break;
                                
                        }
                        else
                        {
                            Console.WriteLine("Библиотека пуста");
                            Console.ReadKey();
                        }


                        break;
                    #endregion
                    case 4:
                        #region Сохранение в файл
                        do
                        {
                            Console.Write("Введите имя файла: ");
                            fileName = Console.ReadLine();

                        } while (fileName.Length < 3);

                        library.SaveLibraryToFile(fileName);

                        Console.WriteLine("Файл успешно сохранен.");
                        Console.ReadKey();

                        break;
                    #endregion
                    case 5:
                        #region Для печати
                        do
                        {
                            Console.Write("Введите имя файла: ");
                            fileName = Console.ReadLine();

                        } while (fileName.Length < 3);

                        library.ExportLibraryToFile(fileName);

                        Console.WriteLine("Сформирован файл для печати");
                        Console.ReadKey();

                        break;

                    #endregion
                    case 6:
                        #region Загрузка с файла

                        do
                        {
                            Console.Write("Введите имя файла: ");
                            fileName = Console.ReadLine();

                        } while (fileName.Length < 3);

                        library.LoadLibraryFromFile(fileName);
                        Console.WriteLine("Библиотека успешно загружена");

                        Console.ReadKey();

                        break;

                    #endregion
                    case 0:
                        #region Выход
                        Environment.Exit(0);

                        break;
                        #endregion

                }

            }
        }   
    }
}
