﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMania
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Выводим N раз число N");
            Console.WriteLine("");

            string povtor;


            try
            {

                do
                {
                    int i = 1;

                    Console.Write("Введите N: ");
                    int N = int.Parse(Console.ReadLine());

                    while (i <= N)
                    {
                        Console.WriteLine($"{N}");
                        i++;
                    }


                    Console.WriteLine("");
                    Console.Write("Введите 'Y', если хотите повторить: ");
                    povtor = Console.ReadLine();
                }
                while (povtor == "Y");

            }
            catch
            {
                Console.WriteLine("Данные неверны. Программа будет закрыта");
            }

            Console.ReadKey();
        }
    }
}
