﻿using System;
using System.IO;

public class Sum
{
    private static void Main()
    {
        string LetterFilter = "ABCEHKMOPTXY";
        string NumberFilter = "0123456789";
        string Letter = "sdf";
        string Number = "xxx";
        bool LetterGood = false, NumberGood = false;


        StreamReader reader = new StreamReader("input.txt");
        string input = reader.ReadToEnd();
        reader.Close();


        StreamWriter writer = new StreamWriter("output.txt");

        string[] separator = { Environment.NewLine };
        string[] inputs = input.Split(separator, StringSplitOptions.RemoveEmptyEntries);



        for (int i = 1; i < inputs.Length; i++)
        {
            Number = inputs[i].Substring(1, 3);
            Letter = inputs[i].Remove(1, 3);

            for (int j = 0; j < 3; j++)
            {
                if (LetterFilter.Contains(Letter[j].ToString()) == true)
                {
                    LetterGood = true;
                }
                else
                {
                    LetterGood = false;
                    break;
                }

            }

            for (int j = 0; j < 3; j++)
            {
                if (NumberFilter.Contains(Number[j].ToString()) == true)
                {
                    NumberGood = true;
                }
                else
                {
                    NumberGood = false;
                    break;
                }
            }

            if (NumberGood == true && LetterGood == true)
            {
                writer.WriteLine("Yes");
            }
            else
            {
                writer.WriteLine("No");
            }

        }


        writer.Close();

    }
}