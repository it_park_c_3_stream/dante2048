﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Palindrom
{
    class Program
    {
        static void Main(string[] args)
        {
            int N, a, b, c, d;


            StreamReader reader = new StreamReader("input.txt");
            N = int.Parse(reader.ReadLine());

            reader.Close();

            StreamWriter writer = new StreamWriter("output.txt");

            if ((1000 <= N) && (N <= 9999))
            {
                a = N / 1000;
                b = N / 100 % 10;
                c = N % 100 / 10;
                d = N % 10;
                if ((a==d)&&(b == c))
                    writer.Write("YES");
                else
                    writer.Write("NO");
            }

            writer.Close();

        }
    }
}
