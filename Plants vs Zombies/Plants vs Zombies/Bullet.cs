﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plants_vs_Zombies
{
    class Bullet:Unit
    {
        private int step;


        public Bullet(int in_x, int in_y, int in_damage, int in_step, ConsoleColor in_statusHP) : base(in_x, in_y, 0, '-', in_damage, in_step, in_statusHP, false)
        {
            step = in_step;
        }

        public bool MoveBullet(int in_step)
        {

            bool result;

            if (X + step < Console.WindowWidth - 5)
            {
                result = true;

                    switch (in_step)
                    {
                    case 1:
                        if (step == 3)
                        {
                            X++;
                        }
                        break;

                    case 2:
                        if (step == 3 || step == 2 || step == 1)
                        {
                            X++;
                        }
                        break;

                    case 3:
                        if (step == 3)
                        {
                            X++;
                        }
                        break;

                    case 4:
                        if (step == 3 || step == 2 || step == 1)
                        {
                            X++;
                        }
                        break;
                    }


            }
            else
            {
                result = false;
            }

            return result;
        }

        public bool HitBullet(List<Zombies> zombies)
        {
            bool hit = false;

            for (int i = 0; i < zombies.Count; i++)
            {
                if (this.X >= zombies[i].X && this.Y == zombies[i].Y || zombies[i].step==1 && this.X >= zombies[i].X && this.Y == zombies[i].Y)
                {
                    zombies[i].Hit(this);
                    hit = true;

                    if (zombies[i].StatusHP == ConsoleColor.Blue)
                    {
                        if (zombies[i].Stop == true)
                        {
                            zombies[i].Stop = false;
                        }
                        else { zombies[i].Stop = true; }
                    }


                    if (zombies[i].Dead())
                    {
                        zombies.RemoveAt(i);

                    }
                    else
                    {
                        i++;
                    }

                }
            }

            return hit;
        }


    }
}
