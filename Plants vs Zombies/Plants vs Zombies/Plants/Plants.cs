﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plants_vs_Zombies
{
    class Plants : Unit
    {
        private int x, y;
        private char face;
        private int hp;
        private int damage;
        private int step;
        private ConsoleColor statusHP;

        protected Plants(int in_x, int in_y, int in_hp, char in_face, int in_damage, int in_step, ConsoleColor in_statusHP) : base(in_x, in_y, in_hp, in_face, in_damage, in_step, in_statusHP, true)
        {
            x = in_x;
            y = in_y;
            face = in_face;
            hp = in_hp;
            damage = in_damage;
            step = in_step;
            statusHP = in_statusHP;
        }

        public void Shoting(List<Bullet> bullets, int in_step)
        {
            bool queue = true;

            switch (in_step)
            {
                case 1:
                    if (step == 2)
                    {
                        for (int i = 0; i < bullets.Count; i++)
                        {
                            if (bullets[i].CheckStep() == 2 && bullets[i].X < X + 4 && bullets[i].Y == Y)
                            {
                                queue = false;
                                break;
                            }
                        }

                        if (queue == true)
                        {
                            bullets.Add(new Bullet(x + 1, y, damage, step, ConsoleColor.DarkYellow));
                        }


                    }
                    if (step == 3)
                    {
                        for (int i = 0; i < bullets.Count; i++)
                        {
                            if (bullets[i].CheckStep() == 3 && bullets[i].X < X + 5 && bullets[i].Y == Y)
                            {
                                queue = false;
                                break;
                            }
                        }

                        if (queue == true)
                        {
                            bullets.Add(new Bullet(x + 1, y, damage, step, ConsoleColor.Red));
                        }


                    }
                    break;


                case 4:
                    if (step == 1)
                    {
                        for (int i = 0; i < bullets.Count; i++)
                        {
                            if (bullets[i].CheckStep() == 1 && bullets[i].X < X + 6 && bullets[i].Y == Y)
                            {
                                queue = false;
                                break;
                            }
                        }

                        if (queue == true)
                        {
                            bullets.Add(new Bullet(x + 1, y, damage, step, ConsoleColor.Blue));
                        }
                        
                    }
                    break;

            }

            
        }

        public bool Danger(List<Zombies> zombies)
        {
            bool result = false;
            for (int i = 0; i < zombies.Count; i++)
            {
                if (Y == zombies[i].Y)
                {
                    result = true;
                    break;
                } 
            }

            return result;
        }

    }
}
