﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plants_vs_Zombies
{
    class SpeedPlants:Plants
    {
        public SpeedPlants(int in_x, int in_y) : base(in_x, in_y, 5, (char)6, 1, 3, ConsoleColor.DarkRed)
        {
        }
    }
}
