﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plants_vs_Zombies
{
    class TankPlants : Plants
    {
        public TankPlants(int in_x, int in_y) : base(in_x, in_y, 10, (char)4, 1, 1, ConsoleColor.Blue)
        {
        }
    }
}
