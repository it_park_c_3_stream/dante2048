﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Plants_vs_Zombies
{
    class Program
    {
        static void Main(string[] args)
        {
            int score = 0;
            bool EndGame = false;
            int step = 0;
            action direction;
            Console.CursorVisible = false;
            Random rnd = new Random();

            Console.SetWindowSize(80, 40);
            Console.SetBufferSize(80, 40);

            Screen screen = new Screen();

            List<Plants> plants = new List<Plants>();
            List<Zombies> zombies = new List<Zombies>();
            List<Bullet> bullets = new List<Bullet>();

            screen.GoToMainMenu();

            plants.Add(new TankPlants(2, 14));
            plants.Add(new SpeedPlants(2, 15));
            plants.Add(new DDPlants(2, 16));
            plants.Add(new DDPlants(2, 17));
            plants.Add(new SpeedPlants(2, 18));
            plants.Add(new TankPlants(2, 19));
            plants.Add(new DDPlants(2, 20));
            plants.Add(new TankPlants(3, 15));


            zombies.Add(new JumpZombies(Console.WindowWidth - 3, rnd.Next(14, 20)));
            zombies.Add(new DDZombies(Console.WindowWidth - 3, rnd.Next(14, 20)));
            zombies.Add(new TankZombies(Console.WindowWidth  - 3, rnd.Next(14, 20)));



            while (EndGame == false)
            {
                step++;

                Console.Clear();

                screen.ShowHeader();
                screen.GameBoard();

                Console.SetCursorPosition(0, 0);

                if (zombies[zombies.Count - 1].X < Console.WindowWidth - rnd.Next(6, 12) && step>2)
                {
                    zombies.Add(new JumpZombies(Console.WindowWidth - 3, rnd.Next(14, 20)));
                    zombies.Add(new DDZombies(Console.WindowWidth - 3, rnd.Next(14, 20)));
                    zombies.Add(new TankZombies(Console.WindowWidth - 3, rnd.Next(14, 20)));
                }

                for (int i = 0; i < zombies.Count; i++)
                {               
                    if (zombies[i].EndGame())
                    {
                        EndGame = true;
                    }
                    else
                    {
                        zombies[i].MoveZombies(plants, step);
                    }
 

                    zombies[i].Draw();
                }


                for (int i = 0; i < plants.Count; i++)
                {
                    plants[i].Draw();

                    if (plants[i].Danger(zombies))
                    {
                        plants[i].Shoting(bullets, step);
                    }


                }

                for (int i = 0; i < bullets.Count;)
                {
                    if (bullets[i].MoveBullet(step) == false || bullets[i].HitBullet(zombies))
                    {
                        bullets.RemoveAt(i);
                    }
                    else
                    {
                        bullets[i].Draw();
                        i++;
                    }
               
                }

                if (screen.GetInput() == action.pause)
                {
                    Console.ReadKey();
                }

                if (step == 4)
                {
                    step = 0;
                }

                if (zombies.Count == 0)
                {
                    EndGame = true;
                }

                Thread.Sleep(100);

            }


            Console.ReadKey();


        }
    }
}
