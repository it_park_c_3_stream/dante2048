﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Plants_vs_Zombies
{
    public enum action {up, down, nun, enter, pause }
     class Screen
    {

        string Item;
        List<string> menuItems;
        int selectMenuItem;
        List<string> complexities = new List<string>() {"Легко", "Средне", "Сложно"};
        int selectСomplexities = 0;
        action direction;

        static ConsoleColor colorMenu;

        public Screen()
        {
            menuItems = new List<string>();
        }

        public void ShowHeader()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            string nameHeader = "PLANTS vs ZOMBIES";
            Console.SetCursorPosition((Console.WindowWidth / 2 - nameHeader.Length/2), 0);
            Console.Write($"{nameHeader}");
        }


        public void ShowMainMenu()
        {
            Console.Clear();

            ShowHeader();

            menuItems.Clear();

            colorMenu = ConsoleColor.DarkRed;
            Console.ForegroundColor = colorMenu;

            menuItems.Add("Начать игру");
            menuItems.Add("Уровень сложности: ");
            menuItems.Add("Правила игры");


            for (int i = 0; i < menuItems.Count; i++)
            {
                Console.SetCursorPosition(3, 3 + i);
                if (i == 1)
                {
                    Item = menuItems[i] + complexities[selectСomplexities];
                }
                else
                {
                    Item = menuItems[i];
                }


                if (selectMenuItem == i)
                {

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(Item);
                    Console.ForegroundColor = colorMenu;

                }
                else
                {
                    Console.WriteLine(Item);
                }


            }
            Console.WriteLine(selectMenuItem);
            Console.WriteLine(selectСomplexities);

        }


        public action GetInput()
        {
            

            if (Console.KeyAvailable)
            {

                ConsoleKeyInfo key = Console.ReadKey();


                if (key.Key == ConsoleKey.DownArrow)
                {
                    return action.down;
                }
                if (key.Key == ConsoleKey.UpArrow)
                {
                    return action.up;
                }
                if (key.Key == ConsoleKey.Enter)
                {
                    return action.enter;
                }
                if (key.Key == ConsoleKey.Spacebar)
                {
                    return action.pause;
                }
            }
            return action.nun;


        }

        public void MenuSelection()
        {

            do
            {
                ShowMainMenu();
                direction = GetInput();
                if (direction == action.down)
                {
                    if (selectMenuItem + 1 == menuItems.Count)
                    {
                        selectMenuItem = 0;
                    }
                    else
                    {
                        selectMenuItem++;
                    }
                }
                if (direction == action.up)
                {
                    if (selectMenuItem - 1 < 0)
                    {
                        selectMenuItem = menuItems.Count - 1;
                    }
                    else
                    {
                        selectMenuItem--;
                    }
                }
                Thread.Sleep(100);
            }
            while (direction != action.enter);

        }

        public void GoToMainMenu()
        {
            do
            {
                MenuSelection();

                if (selectMenuItem == 1)
                {
                    if (selectСomplexities + 1 == complexities.Count)
                    {
                        selectСomplexities = 0;
                    }
                    else
                    {
                        selectСomplexities++;
                    }

                }
                if (selectMenuItem == 2)
                {
                    RulesOfPlay();
                }
            }
            while (selectMenuItem != 0);





        }


        private void RulesOfPlay()
        {
            Console.Clear();

            ShowHeader();

            menuItems.Clear();

            do
            {


                Console.SetCursorPosition(3, 3);
                Console.WriteLine("Суть данной игры заключается в том, что бы удержать защиту против волн зомби.");
                Console.WriteLine("Игра считается проигранной, если зомби прошли оборонительный кардон.");
                Console.WriteLine(" ");
                Console.WriteLine("Виды юнитов на карте: ");
                Console.WriteLine((char)6 + " - Скоростной цветок. Он растреливает как пулемет. Очень эффективен против бысрых зомби.");
                Console.WriteLine((char)4 + " - Цветок - танк. Незначительный урон компенсирует то, что данный цветок устойчив к атакам зомби и замедляет их ход.");
                Console.WriteLine((char)5 + " - Цветок - нагибатор. Имея стандартную скорость выстрела, наносит значительный урон по противнику.");
                Console.WriteLine(" ");
                Console.WriteLine(" ");
                Console.WriteLine((char)11 + " - Конор Мак Зомби. Лучше этого парня не подпускать к себе. Имеет высокий урон.");
                Console.WriteLine((char)2 + " - Зомби - ВДВешник. Имея крепкий чердак, способен выдержать большое колличество урона.");
                Console.WriteLine((char)24 + " - Зомби - Флэш. Этого парня уронили в детстве в котел с RED Bull-ом и теперь у него шило в жопе.");

                Console.WriteLine(" ");
                Console.WriteLine(" ");
                Console.WriteLine(" ");

                menuItems.Clear();

                colorMenu = ConsoleColor.Cyan;
                Console.ForegroundColor = colorMenu;

                menuItems.Add("Вернуться в главное меню");

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(menuItems[0]);
                    Console.ForegroundColor = colorMenu;

                direction = GetInput();
            }
            while (direction != action.enter);

            GoToMainMenu();
        }


        public void GameBoard()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;

            for (int i = 0; i < Console.WindowWidth; i++)
            {
                Console.SetCursorPosition(i, 12);
                Console.Write('*');
                Console.SetCursorPosition(i, 24);
                Console.Write('*');
            }

            for (int i = 13; i < 24; i++)
            {
                Console.SetCursorPosition(0, i);
                Console.Write('*');

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.SetCursorPosition(1, i);
                Console.Write('_');
                Console.ForegroundColor = ConsoleColor.DarkGreen;


                Console.SetCursorPosition(Console.WindowWidth - 1, i);
                Console.Write('*');
            }
        }

    }
}
