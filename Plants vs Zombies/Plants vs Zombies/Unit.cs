﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Plants_vs_Zombies
{
    class Unit
    {
        private int x, y;
        private char face;
        private int hp;
        private int damage;
        private int step;
        private ConsoleColor statusHP;
        private bool stop;
        private List<Unit> units = new List<Unit>();

        public Unit(int in_x, int in_y, int in_hp, char in_face, int in_damage, int in_step, ConsoleColor in_statusHP, bool in_stop)
        {
            x = in_x;
            y = in_y;
            hp = in_hp;
            face = in_face;
            damage = in_damage;
            step = in_step;
            statusHP = in_statusHP;
            stop = in_stop;
        }

        public int X
        {
            set { x = value; }
            get { return x; }
        }

        public int Y
        {
            set { y = value; }
            get { return y; }
        }

        public void Draw()
        {
            Console.SetCursorPosition(x, y);
            Console.ForegroundColor = statusHP;
            Console.Write(face);
            Console.ResetColor();



        }

        public bool Dead()
        {
            if (hp <= 0)
            {
                Console.SetCursorPosition(x, y);
                Console.ForegroundColor = ConsoleColor.Black;
                Console.Write(face);
                Console.ResetColor();
            }
            return hp <= 0;
        }

        public void Hit(Unit unit)
        {
            hp -= unit.damage;

                if (unit.CheckStep() == 1)
                {
                    statusHP = ConsoleColor.Blue;
                }
                else if (hp == 1)
                {
                    statusHP = ConsoleColor.Red;
                }
                else if (hp < 3)
                {
                    statusHP = ConsoleColor.Yellow;
                }
                else if (hp <= 5)
                {
                    statusHP = ConsoleColor.Green;
                }
                else if (hp <= 7)
                {
                    statusHP = ConsoleColor.DarkGreen;
                }


            Console.ForegroundColor = ConsoleColor.Red;
            Console.SetCursorPosition(x, y);
            Console.WriteLine(face);
            Thread.Sleep(100);
            this.Draw();

        }

        public bool EndGame()
        {
            return x <= 1;
        }


        public int CheckStep()
        {
            return step;
        }

        public ConsoleColor StatusHP
        {
            set { statusHP = value; }
            get { return statusHP; }
        }

        public bool Stop
        {
            set { stop = value; }
            get { return stop; }
        }

    }
}
