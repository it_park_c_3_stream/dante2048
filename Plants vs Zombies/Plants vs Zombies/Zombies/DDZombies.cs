﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plants_vs_Zombies
{
    class DDZombies:Zombies
    {
        public DDZombies(int in_x, int in_y) : base(in_x, in_y, 5, (char)11, 2, 2, ConsoleColor.DarkGreen)
        {
        }
    }
}
