﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plants_vs_Zombies
{
    class JumpZombies : Zombies
    {
        public JumpZombies(int in_x, int in_y) : base(in_x, in_y, 5, (char)24, 1, 1, ConsoleColor.DarkGreen)
        {
        }
    }
}
