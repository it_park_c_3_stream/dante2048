﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plants_vs_Zombies
{
    class TankZombies:Zombies
    {
        public TankZombies(int in_x, int in_y) : base(in_x, in_y, 10, (char)2, 1, 3, ConsoleColor.DarkGray)
        {
        }
    }
}
