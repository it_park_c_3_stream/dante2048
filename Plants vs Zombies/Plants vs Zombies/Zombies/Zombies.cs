﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Plants_vs_Zombies
{
    class Zombies:Unit
    {
        public int step;


        protected Zombies(int in_x, int in_y, int in_hp, char in_face, int in_damage, int in_step, ConsoleColor in_statusHP) : base(in_x, in_y, in_hp, in_face, in_damage, in_step, in_statusHP, false)
        {
            step = in_step;
        }

        public void MoveZombies(List<Plants> plants, int in_step)
        {
            if (X > 0)
            {
                for (int i = 0; i < plants.Count; i++)
                {
                    if (X - 1 <= plants[i].X && Y == plants[i].Y)
                    {
                        Stop = true;
                        plants[i].Hit(this);
                        if (plants[i].Dead())
                        {
                            Stop = false;
                        } 
                    }
                }

                if (Stop == false)
                {
                    

                    switch (in_step)
                    {
                        case 1:
                            if (step == 1)
                            {
                                X--;
                            }
                            break;
                        case 3:
                            if (step == 1 || step == 2 || step == 3)
                            {
                                X--;
                            }
                            break;

                    }

                }                
            }
        }

    }
}
