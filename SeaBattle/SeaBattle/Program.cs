﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeaBattle
{
    class Program
    {
        enum Cell { away, ship, empty, hit }
        enum Status { play, winUser, winBot }

        static void Main(string[] args)
        {
          #region Переменные
          const int sizeField = 10;
          Cell[,] field = new Cell[sizeField, sizeField];
          int inputI = -1, inputJ = -1;
          int inputX = -1, inputY = -1;
          Status gameStatus = Status.play;
          bool parseResultI = false;
          bool parseResultJ = false;
          bool upEmpty = false, downEmpty = false, leftEmpty = false, rightEmpty = false;
          bool upSide = false, downSide = false, leftSide = false, rightSide = false;
          bool direction = false;
          int boat = 0, destroyer = 0, cruiser = 0, battleship = 0;
          #endregion



            #region Заполение поля Игроком

            for (int i = 0; i < sizeField; i++)
            {
                for (int j = 0; j < sizeField; j++)
                {
                    field[i, j] = Cell.empty;
                }
            }

            while (boat <= 4 && destroyer <= 3 && cruiser <= 2 && battleship <= 1)
            {
                Console.Clear();

                for (int i = 0; i < sizeField; i++)
                {
                    for (int j = 0; j < sizeField; j++)
                    {
                        switch (field[i, j])
                        {
                            case Cell.away:
                                Console.Write("O");
                                break;
                            case Cell.ship:
                                Console.Write("K");
                                break;
                            case Cell.empty:
                                Console.Write("-");
                                break;
                            case Cell.hit:
                                Console.Write("X");
                                break;

                        }

                    }
                    Console.WriteLine();
                }


                Console.WriteLine($"Установленных кораблей");
                Console.WriteLine($"1. Катера {boat} из 4 ");
                Console.WriteLine($"2. Эсминцы  {destroyer} из 3 ");
                Console.WriteLine($"3. Крейсера {cruiser} из 2 ");
                Console.WriteLine($"4. Линкор {battleship} из 1 ");

                Console.Write("Выберите, какой корабль необходимо установить: ");
                int ChoiceShip = int.Parse(Console.ReadLine());

                if (ChoiceShip >= 1 && ChoiceShip <= 4)
                {


                    Console.WriteLine("Задайте место расположения");

                    do
                    {



                        Console.WriteLine("Введите строку (1-10): ");
                        parseResultI = int.TryParse(Console.ReadLine(), out inputI);
                        if (parseResultI == true) { inputI--; }

                        Console.WriteLine("Введите столбец (1-10): ");
                        parseResultJ = int.TryParse(Console.ReadLine(), out inputJ);
                        if (parseResultJ == true) { inputJ--; }
                    }
                    while
                    (
                      parseResultI == false || parseResultJ == false ||
                      inputI < 0 || inputI > sizeField - 1 ||
                      inputJ < 0 || inputJ > sizeField - 1 ||
                      field[inputI, inputJ] != Cell.empty || 
                      field[inputI - 1, inputJ - 1] != Cell.empty ||
                      field[inputI - 1, inputJ] != Cell.empty ||
                      field[inputI - 1, inputJ + 1] != Cell.empty ||
                      field[inputI, inputJ + 1] != Cell.empty ||
                      field[inputI + 1, inputJ + 1] != Cell.empty ||
                      field[inputI + 1, inputJ] != Cell.empty ||
                      field[inputI + 1, inputJ - 1] != Cell.empty ||
                      field[inputI, inputJ - 1] != Cell.empty
                    );

                    if ((inputI - ChoiceShip + 1) >= 0)
                    {
                        upSide = true;
                    }

                    if ((inputJ + ChoiceShip - 1) <= sizeField - 1)
                    {
                        rightSide = true;
                    }

                    if ((inputJ - ChoiceShip + 1) >= 0)
                    {
                        leftSide = true;
                    }

                    if ((inputI + ChoiceShip - 1) <= sizeField - 1)
                    {
                        downSide = true;
                    }
                   
                    for (int i = 0; i < ChoiceShip; i++)
                    {
                     

                        if (upSide == true && inputJ + 1 <= sizeField - 1)
                        {
                            if (field[inputI - 1 - i, inputJ] == Cell.empty ||
                                field[inputI - 1 - i, inputJ + 1] == Cell.empty ||
                                field[inputI - i, inputJ + 1] == Cell.empty)
                            {
                                upEmpty = true;
                            }
                        }


                        if (rightSide == true && rightSide == true)
                        {
                            if (field[inputI, inputJ + 1 + i] == Cell.empty)
                            {
                                rightEmpty = true;
                            }
                        }

                        if (field[inputI + 1 + i, inputJ - 1] == Cell.empty ||
                           field[inputI + 1 + i, inputJ] == Cell.empty ||
                           field[inputI + 1 + i, inputJ + 1] == Cell.empty)
                        {
                            downEmpty = true;
                        }

                        if (field[inputI - 1, inputJ - 1 - i] != Cell.empty ||
                            field[inputI, inputJ - 1 - i] != Cell.empty ||
                            field[inputI + 1, inputJ - 1 - i] != Cell.empty)
                        {
                            leftEmpty = true;
                        }

                        
                    }

                    if (upEmpty == false && downEmpty == false && leftEmpty == false && rightEmpty == false)

                    {
                        Console.WriteLine("Данные координаты нарушают правила игры!                 Повторите набор");
                        Console.ReadKey();
                    }
                    else
                    {
                        do
                        {

                            Console.WriteLine("Выберите доступный вариант направления корабля:");

                            Console.WriteLine();
                            Console.Write(" 1. Вверх");
                            if (upEmpty == false)
                            {
                                Console.Write(" - Недоступен");
                            }

                            Console.WriteLine();
                            Console.Write(" 2. Вниз");
                            if (downEmpty == false)
                            {
                                Console.Write(" - Недоступен");
                            }

                            Console.WriteLine();
                            Console.Write(" 3. Влево");
                            if (leftEmpty == false)
                            {
                                Console.Write(" - Недоступен");
                            }

                            Console.WriteLine();
                            Console.Write(" 4. Вправо");
                            if (rightEmpty == false)
                            {
                                Console.Write(" - Недоступен");
                            }

                            Console.WriteLine();

                            if (ChoiceShip == 1)
                            {
                                boat++;
                                field[inputI, inputJ] = Cell.ship;
                            }

                            if (ChoiceShip == 2)
                            {
                                destroyer++;
                            }
                            if (ChoiceShip == 3)
                            {
                                cruiser++;
                            }
                            if (ChoiceShip == 4)
                            {
                                battleship++;
                            }


                            int ChoiceDirection = int.Parse(Console.ReadLine());

                            switch (ChoiceDirection)
                            {
                                case 1:
                                    if (upEmpty == true)
                                    {
                                        for (int i = 0; i < ChoiceShip; i++)
                                        {
                                            field[inputI - i, inputJ] = Cell.ship;
                                        }
                                    }
                                    else
                                    {
                                        goto default;
                                    }
                                    break;
                                case 2:
                                        if (downEmpty == true)
                                        {
                                            for (int i = 0; i < ChoiceShip; i++)
                                            {
                                            field[inputI + i, inputJ] = Cell.ship;
                                                direction = true;
                                            }
                                        }
                                        else
                                        {
                                            goto default;
                                        }
                                        break;
                                case 3:
                                        if (leftEmpty == true)
                                        {
                                                for (int i = 0; i < ChoiceShip; i++)
                                                {
                                                    field[inputI, inputJ - i] = Cell.ship;
                                                    direction = true;
                                                }
                                        }
                                        else
                                        {
                                            goto default;
                                        }
                                        break;
                                case 4:
                                        if (rightEmpty == true)
                                        {
                                                for (int i = 0; i < ChoiceShip; i++)
                                                {
                                                field[inputI, inputJ + i] = Cell.ship;
                                                    direction = true;
                                                }
                                        }
                                        else
                                        {
                                            goto default;
                                        }
                                        break;
                                default:
                                        Console.WriteLine("Ошибка при выборе! Повторите                             попытку");
                                        Console.ReadKey();
                                        direction = false;
                                        break;
                            }

                        } while (direction == false);

                    }

                }

            }

            Console.ReadKey();
        
    
            #endregion

            //#region Заполнение поля компьютером
            //for (int x = 0; x < sizeField; x++)
            //{
            //    for (int y = 0; y < sizeField; y++)
            //    {
            //        field[x, y] = Cell.empty;
            //    }
            //}
            //#endregion


            //#region Игровой цикл
            //while (gameStatus == Status.play)
            //{

            //    #region Вывод полей на экран
            //    Console.Clear();
            //    for (int i = 0; i < sizeField; i++)
            //    {
            //        for (int j = 0; j < sizeField; j++)
            //        {
            //            switch (field[i, j])
            //            {
            //                case Cell.away:
            //                    Console.Write("O");
            //                    break;
            //                case Cell.ship:
            //                    Console.Write("K");
            //                    break;
            //                case Cell.empty:
            //                    Console.Write("-");
            //                    break;
            //                case Cell.hit:
            //                    Console.Write("X");
            //                    break;
                            
            //            }

            //        }
            //        Console.WriteLine();
            //    }

            //    for (int x = 0; x < sizeField; x++)
            //    {
            //        for (int y = 0; y < sizeField; y++)
            //        {
            //            switch (field[x, y])
            //            {
            //                case Cell.away:
            //                    Console.Write("O");
            //                    break;
            //                case Cell.empty:
            //                    Console.Write("-");
            //                    break;
            //                case Cell.hit:
            //                    Console.Write("X");
            //                    break;

            //            }

            //        }
            //        Console.WriteLine();
            //    }
            //    #endregion






            //}
            //#endregion







            
        }
    }
}
