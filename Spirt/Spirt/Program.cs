﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Spirt
{
    class Program
    {
        static void Main(string[] args)
        {
            int C, H, O;


            StreamReader reader = new StreamReader("input.txt");
            C = int.Parse(reader.ReadLine());
            H = int.Parse(reader.ReadLine());
            O = int.Parse(reader.ReadLine());
            reader.Close();

            StreamWriter writer = new StreamWriter("output.txt");

            int x, y;
            if ((C % 2 == 0) && (H % 6 == 0) && (O % 1 == 0))
            {
                x = C / 2;
                y = H / 6;
                if ((x < y) && (y < O)) writer.Write($"Молекул спирта:{x}");
                if ((y < x) && (y < O)) writer.Write($"Молекул спирта:{y}");
                if ((O < x) && (O < y)) writer.Write($"Молекул спирта:{y}");
            }
            else
            {
                writer.Write("Молекулы спирта отсутствуют ");
            }
            writer.Close();

        }
    }
}
