﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubarrayArray
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.Write("Введите колличество элементов массива: ");
            int n = int.Parse(Console.ReadLine());

            int[] mas = new int[n];

            Console.WriteLine("Заполните массив");
            for (int a = 0; a < n; a++)
            {
                Console.Write($"Значение №{a+1}: ");
                mas[a] = int.Parse(Console.ReadLine());  

            }
            Console.Write("Массив: ");
            for (int a = 0; a < n; a++)
            {
                Console.Write($"{mas[a]} ");
                

            }
            Console.WriteLine("");

            Console.Write("Введите колличество необходимых подмассивов: ");
            int m = int.Parse(Console.ReadLine());

            for (int b = 0; b < m; b++)
            {
                Console.Write("Введите начальный элемент подмассива: ");
                int i = int.Parse(Console.ReadLine());

                Console.Write("Введите конечный элемент подмассива: ");
                int j = int.Parse(Console.ReadLine());

                Console.Write("Найденные значения в рамках указанного диапозона: ");


                for (; i <= j; i++)
                {
                    Console.Write($"{mas[i-1]} ");


                }
                Console.WriteLine("");
            }

            Console.ReadKey();
        }
    }
}
