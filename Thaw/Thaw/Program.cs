﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thaw
{
    class Program
    {
        static void Main(string[] args)
        {
            int day = 0;
            int midlTemp;
            int presentDay = 0;
            int period = 0;
            int result = 0;

            Console.WriteLine("Выявление самой продолжительной оттепели");
            Console.WriteLine("");

            Console.Write("Введите колличество дней для статистики: ");
            day = int.Parse(Console.ReadLine());

            do
            {
                presentDay++;

                Console.Write($"{presentDay} День. Температура : ");
                midlTemp = int.Parse(Console.ReadLine());

                if (-50 <= midlTemp && midlTemp <= 50)
                {
                    if (midlTemp >0)
                    {
                        period++;
                    }
                    else
                    {
                        if (result < period)
                        {
                            result = period;
                            period = 0;
                        }
                    }


                }
                else
                {

                    Console.WriteLine("Введенная температура не соответствует действительности. Повторите ввод");

                    presentDay--;

                }
            }
            while (presentDay < day);


            Console.Write($"Длина самой продолжительной оттепели: {result}");

            Console.ReadKey();


        }
    }
}
