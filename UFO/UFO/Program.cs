﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace UFO
{
    class Program
    {
        static void Main(string[] args)
        {
            int r1, r2, r3;


            StreamReader reader = new StreamReader("input.txt");
            r1 = int.Parse(reader.ReadLine());
            r2 = int.Parse(reader.ReadLine());
            r3 = int.Parse(reader.ReadLine());
            reader.Close();

            StreamWriter writer = new StreamWriter("output.txt");

            if (1 <= r1)
                if ((r2 + r3) <= r1)  writer.Write("YES");
            else
            {
                writer.Write("NO");
            }
            writer.Close();

        }
    }
}
