﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ugadaika
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int winNumber;
            int rndNumber = 0;
            int complexity;
            int truing = 0;
            int rangeMax = 10, rangeMin = 0;
            int truingAvailable = 0;
            string Message = "";
            string complexityMessage = "";
            bool continueGame = true;

            Console.ForegroundColor = ConsoleColor.Cyan;

            try
            {
                
                do
                {
                
                do
                {

                    Console.Clear();

                    Console.WriteLine("Выберите уровень сложности");
                    Console.WriteLine("1. Новичок - Угадать число от 0 до 10 имея 5 попыток");
                    Console.WriteLine("2. Профи - Угадать число от 0 до 100 имея 10 попыток");
                    Console.WriteLine("3. Мастер - Угадать число от 0 до 1000 имея 20 попыток");
                    Console.WriteLine("4. Читер - Угадать число из заданного диапозона,  имея 5 попыток");


                    Console.Write("Ваш вариант сложности: ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    complexity = int.Parse(Console.ReadLine());

                    switch (complexity)
                    {
                        case 1:

                            rndNumber = rnd.Next(0, 10 + 1);
                            complexityMessage = "Новичок";
                            truingAvailable = 5;
                            break;
                        case 2:
                            rangeMax = 100;
                            rndNumber = rnd.Next(0, rangeMax + 1);
                            complexityMessage = $"Профи";
                            truingAvailable = 10;
                            break;
                        case 3:
                            rangeMax = 1000;
                            rndNumber = rnd.Next(0, rangeMax + 1);
                            complexityMessage = "Мастер";
                            truingAvailable = 20;
                            break;
                        case 4:
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.Write("От: ");
                            Console.ForegroundColor = ConsoleColor.Red;
                            rangeMin = int.Parse(Console.ReadLine());
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.Write("До: ");
                            Console.ForegroundColor = ConsoleColor.Red;
                            rangeMax = int.Parse(Console.ReadLine());
                            rndNumber = rnd.Next(rangeMin, rangeMax + 1);
                            complexityMessage = $"Читер";
                            truingAvailable = 5;
                            break;
                        default:
                            complexity = 0;
                            Console.WriteLine("Вам нужно выбрать из трех предложенных вариантов");
                            Console.WriteLine("Для Выбора нажмите Любую Клавишу");
                            Console.ReadKey();
                            break;
                    }

                }
                while (complexity == 0);



                truing = 0;


                do
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"Ваш уровень сложности: {complexityMessage}");
                    Console.WriteLine();

                    truing++;

                    if (truing >= 2)
                    {
                        Console.WriteLine();
                        Console.WriteLine($"{Message}");
                        Console.WriteLine();

                    }

                    Console.WriteLine($"Угадайте загаданное число - от {rangeMin} до {rangeMax}");
                    Console.WriteLine();


                    Console.WriteLine($"Попытка №{truing} из {truingAvailable}-ти доступных");


                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write("Ваш вариант: ");
                    Console.ForegroundColor = ConsoleColor.Red;
                    winNumber = int.Parse(Console.ReadLine());


                    if (rangeMin < winNumber && winNumber < rndNumber)
                    {
                        rangeMin = winNumber;
                        Message = $"Загаданное число больше {winNumber}";

                    }
                    else if (rangeMax > winNumber && winNumber > rndNumber)
                    {
                        rangeMax = winNumber;
                        Message = $"Загаданное число меньше {winNumber}";
                    }
                    else
                    {
                        Message = $"{winNumber} не входит в заданный диапозон";
                    }



                }
                while (winNumber != rndNumber & truing < truingAvailable);

                Console.ForegroundColor = ConsoleColor.Cyan;

                if (truing <= truingAvailable)
                {
                    Console.WriteLine("УРА!!! Вы угадали число");
                    Console.WriteLine($"Попыток понадобилось: {truing} ");

                    if (truing == 1) { Console.WriteLine("Звание: Снайпер"); }
                    if (truing == truingAvailable) { Console.WriteLine("Звание: Канатоходец"); }

                }
                else { Console.WriteLine($"Вы исчерпали все попытки... Ваша Игра окончена"); }

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Хотите сыграть ещё раз? (y/n): ");
                char answer = char.Parse(Console.ReadLine());

                continueGame = answer == 'y' ? true : false;

            }
            while (continueGame == true);

            Console.WriteLine("Спасибо за Игру");
            
            }
            catch
            {
                Console.WriteLine("Данные неверны. Программа будет закрыта");
            }
        
            Console.ReadKey();
        }
    }
}
    
