﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vklad
{
    class Program
    {
        static void Main(string[] args)
        {
            double vklad;
            double platesh = 0;
            int i=0;
            int month = 3;

            Console.WriteLine("Гражданин внес Вклад");
            vklad = 1000;

            do
            {
                platesh = vklad / 100 * 2;
                vklad += platesh;

                if (platesh <= 100)
                {
                    if (month < 12)
                    {
                        month++;
                    }
                    else
                    {
                        month = 1;
                    }

                }

                if (vklad <= 1200)
                {
                    i++;
                }


            } while ((vklad <= 1200) || (platesh <= 100));

            Console.WriteLine($"В {month} месяце увеличение привысило 30 рублей составяет {platesh}");
            Console.WriteLine($"Через {i} месяцев вклад привысил 1200 рублей и составяет {vklad}");

            Console.ReadKey();

        }
    }
}
