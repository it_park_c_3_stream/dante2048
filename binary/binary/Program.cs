﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace binary
{
    class Program
    {
        static void Main(string[] args)
        {
            double N, X = 0;
            int K = 1;

            StreamReader reader = new StreamReader("input.txt");
            N = int.Parse(reader.ReadLine());

            reader.Close();

            StreamWriter writer = new StreamWriter("output.txt");

            while (X < N)
            {
                X = Math.Pow(2,K);
                K++;
            }

            if (X==N)
                writer.Write("YES");
            if (X > N)
                writer.Write("NO");

            writer.Close();


        }
    }
}
