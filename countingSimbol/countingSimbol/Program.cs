﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace countingSimbol
{
    class Program
    {
        static void Main(string[] args)
        {
            string vowel = "йцкнгшщзхфвпрлджчсмтб";
            string consonant = "уеыаоэяию";
            string number = "0123456789";
            int CountVowel = 0, CountConsonant = 0, CountNumber = 0;
            string text = String.Empty;

            Console.WriteLine("Введите текст");
            text = Console.ReadLine();

            text = text.ToLower();

            for (int i = 0; i < text.Length; i++)
            {
                if (vowel.Contains(text[i].ToString()) == true)
                {
                    CountVowel ++;
                }
                if (consonant.Contains(text[i].ToString()) == true)
                {
                    CountConsonant ++;
                }
                if (number.Contains(text[i].ToString()) == true)
                {
                    CountNumber ++;
                }
            }
            Console.WriteLine($"Колличество гласных{CountVowel}. Колличество согласных{CountConsonant}. Колличество цифр{CountNumber}");
            Console.ReadKey();
        }
    }
}
