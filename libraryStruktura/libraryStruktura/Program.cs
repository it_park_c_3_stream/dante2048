﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace libraryStruktura
{
    class Program
    {

        #region Динамический массив

        static Book[] ResizeDArray(Book[] oldArray, int newLeght)
        {
            Book[] newArray = new Book[newLeght];
            if (newArray.Length > oldArray.Length)
            {
                for (int i = 0; i < oldArray.Length; i++)
                {
                    newArray[i] = oldArray[i];
                }
            }
            else
            {
                for (int i = 0; i < newArray.Length; i++)
                {
                    newArray[i] = oldArray[i];
                }
            }

            return newArray;

        }


        #endregion

        #region описание структуры

        struct Book
        {
            public string name;
            public string author;
            public int publicationData;
            public double rating;
            public char location;
        }

        #endregion

        #region основные методы работы с массивом структуры

        static Book[] CreateEmptyLibrary()
        {
            return new Book[] { };
        }

        static void PrintBook(int number, Book book)
        {
            Console.WriteLine("{0,-4}{1,-10}{2,-7}{3,-12}{4,-8}{5,-10}", number, book.name, book.author, book.publicationData, book.rating, book.location);
        }

        static void PrintLibrary(Book[] library)
        {
            Console.WriteLine("{0,-4}{1,-10}{2,-7}{3,-12}{4,-8}{5,-10}", "#", "Name", "Author", "Publication", "Rating", "Location");

            for (int i = 0; i < library.Length; i++)
            {
                PrintBook(i + 1, library[i]);
            }

            Console.WriteLine();
            Console.WriteLine(new string('-', 40));
            Console.WriteLine();
        }

        static Book InputNewBook(Book[] library)
        {
            Book book;
            bool result = true;

            do
            {
                Console.WriteLine("Введите название книги");
                book.name = Console.ReadLine();

                for (int i = 0; i < library.Length; i++)
                {
                    if (book.name == library[i].name)
                    {
                        Console.WriteLine("Книга с таким названием уже есть");
                        result = false;
                        break;
                    }
                }


            } while (book.name.Length < 2 || result == false);
       

            do
            {
                Console.WriteLine("Введите автора");
                book.author = Console.ReadLine();

            } while (book.author.Length < 2);

            do
            {
                Console.WriteLine("Введите год пибликации книги");
                result = int.TryParse(Console.ReadLine(), out book.publicationData);

            } while (result==false || book.publicationData <= 0);

            do
            {
                Console.WriteLine("Введите рейтинг книги");
                result = double.TryParse(Console.ReadLine(), out book.rating);

            } while (result==false || book.rating < 0);

            book.location = 'Б';



            return book;

        }

        static void AddBookToLibrary(ref Book[] library, Book book)
        {
            library = ResizeDArray(library, library.Length + 1);
            library[library.Length - 1] = book;
        }

        static void DeleteBookFromLibraryByNumber(ref Book[] library, int number)
        {
            int index = number - 1;

            for (int i = index; i < library.Length - 1; i++)
            {
                library[i] = library[i + 1];
            }

            library = ResizeDArray(library, library.Length - 1);
        }

        static void DeleteBookFromLibraryByName(ref Book[] library, string nameBook)
        {
            int number = 0; 

            for (int i = 0; i < library.Length; i++)
            {
                if (nameBook==library[i].name)
                {
                    number = i + 1;
                    break;
                }
            }

            if (number>0)
            {
                DeleteBookFromLibraryByNumber(ref library, number);
                Console.WriteLine("Книга удалена из библиотеки");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Название данной книги отсутствует в библиотеке");
                Console.ReadKey();
            }
            
        }

        static void UpdateBookFromLibraryByNumber(Book[] library, int number, Book book)
        {
            int index = number - 1;
            library[index] = book;
        }

        static void UpdateBookFromLibraryByName(Book[] library, string nameBook, Book book)
        {
            int number = 0;

            for (int i = 0; i < library.Length; i++)
            {
                if (nameBook == library[i].name)
                {
                    number = i + 1;
                    break;
                }
            }

            if (number > 0)
            {
                UpdateBookFromLibraryByNumber(library, number, book);
                Console.WriteLine("Книга изменена");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Название данной книги отсутствует в библиотеке");
                Console.ReadKey();
            }

            
        }

        static void InsertBookToLibraryByNumber(ref Book[] library, int number, Book book)
        {
            int index = number - 1;

            library = ResizeDArray(library, library.Length + 1);
            for (int i = library.Length - 1; i > index; i--)
            {
                library[i] = library[i - 1];
            }

            library[index] = book;
        }

        //static Book SearchTopBookByRating(Book[] library)
        //{
        //    string topBookName;
        //    double topRating = 0;

        //    for (int i = 0; i < library.Length -1 ; i++)
        //    {
        //        if (library[i].rating > topRating)                
        //        {
        //            topBookName = library[i].name;
        //        }

        //    }


        //    return topBookName;
        //}




        #endregion

        #region дополительые методы работы с массивом структуры

        static void SaveLibraryToFile(Book[] library, string fileName)
        {
            StreamWriter writer = new StreamWriter(fileName);
            writer.WriteLine(library.Length);

            for (int i = 0; i < library.Length; i++)
            {
                writer.WriteLine(library[i].name);
                writer.WriteLine(library[i].author);
                writer.WriteLine(library[i].publicationData);
                writer.WriteLine(library[i].rating);
                writer.WriteLine(library[i].location);
            }
            writer.Close();
        }

        static void PrintBookToFile(int number, Book book, StreamWriter writer)
        {
            writer.WriteLine("{0,-4}{1,-10}{2,-7}{3,-12}{4,-8}{5,-10}", number, book.name, book.author, book.publicationData, book.rating, book.location);
        }

        static void ExportLibraryToFile(Book[] library, string fileName)
        {
            StreamWriter writer = new StreamWriter(fileName);

            writer.WriteLine("Список книг в библиотеке");
            writer.WriteLine(new string('-', 40));
            writer.WriteLine();

            writer.WriteLine("{0,-4}{1,-10}{2,-7}{3,-12}{4,-8}{5,-10}", "#", "Name", "Author", "Publication", "Rating", "Location");

            for (int i = 0; i < library.Length; i++)
            {
                PrintBookToFile(i+1, library[i], writer);
            }

            writer.WriteLine();
            writer.WriteLine(new string('-', 40));
            writer.WriteLine();

            writer.Close();
        }

        static Book[] LoadLibraryFromRile(string fileName)
        {
            StreamReader reader = new StreamReader(fileName);
            int length = int.Parse(reader.ReadLine());

            Book[] library = new Book[length];

            for (int i = 0; i < library.Length; i++)
            {
                library[i].name = reader.ReadLine();
                library[i].author = reader.ReadLine();
                library[i].publicationData = int.Parse(reader.ReadLine());
                library[i].rating = double.Parse(reader.ReadLine());
                library[i].location = char.Parse(reader.ReadLine());
            }
            reader.Close();

            return library;
        }

        #endregion

        #region Интерфейс Программы

        static void ShowHeader()
        {
            Console.WriteLine("Список книг в библиотеке");
            Console.WriteLine(new string('-', 40));
            Console.WriteLine();
        }

        static void ShowMenu()
        {
            Console.WriteLine("Меню:");
            Console.WriteLine("1.Добавить новую книгу в библиотеку");
            Console.WriteLine("2.Удалить книгу из библиотеки");
            Console.WriteLine("3.Редактировать все параметры книги");
            Console.WriteLine("4.Сохранить каталог в файл");
            Console.WriteLine("5.Экспорт каталога для печати");
            Console.WriteLine("6.Загрузить каталог из файла");

            Console.WriteLine("0.Выход из программы");
        }

        static int ChoseMenuItem()
        {
            bool result;
            int item;

            do
            {
                Console.Write("Введите номер пункта меню: ");
                result = int.TryParse(Console.ReadLine(), out item);

                if (result == false || item < 0 || item > 7)
                {
                    Console.WriteLine("Неверный ввод, повторите попытку");
                }

            } while (result == false || item < 0 || item > 7);

            return item;
        }

        #endregion

        static void Main(string[] args)
        {
            Book[] library = CreateEmptyLibrary();
            Book tempBook;
            int numberBook;
            string nameBook;
            bool result = true;
            string fileName;
            int choose;
            int chooseAdd;
            int chooseDelete;
            int chooseUpdate;

            while (true)
            {
                Console.Clear();

                ShowHeader();

                PrintLibrary(library);

                ShowMenu();

                choose = ChoseMenuItem();

                switch (choose)
                {
                    case 1:
                        #region Добавление

                        Console.WriteLine("Выберите способ добавления: ");
                        Console.WriteLine();
                        Console.WriteLine("  1. В конец: ");
                        Console.WriteLine("  2. В выбранную позицию: ");

                        do
                        {
                            chooseAdd = ChoseMenuItem();
                            if (chooseAdd>2)
                            {
                                Console.WriteLine("Неверный ввод, повторите попытку");
                            }

                        } while (chooseAdd> 2);


                        switch (chooseAdd)
                        {
                            case 1:
                                tempBook = InputNewBook(library);
                                AddBookToLibrary(ref library, tempBook);
                                break;
                            case 2:
                                tempBook = InputNewBook(library);
                                do
                                {
                                    Console.Write("Введите номер для вставки: ");
                                    result = int.TryParse(Console.ReadLine(), out numberBook);
                                } while (result == false || numberBook < 1 || numberBook > library.Length);
                                InsertBookToLibraryByNumber(ref library, numberBook, tempBook);
                                break;
                        }
                        

                        break;
                    #endregion
                    case 2:
                        #region Удаление
                        if (library.Length > 0)
                        {


                            Console.WriteLine("Выберите способ удаления: ");
                            Console.WriteLine();
                            Console.WriteLine("  1. По номеру: ");
                            Console.WriteLine("  2. По названию: ");

                            do
                            {
                                chooseDelete = ChoseMenuItem();
                                if (chooseDelete > 2)
                                {
                                    Console.WriteLine("Неверный ввод, повторите попытку");
                                }

                            } while (chooseDelete > 2);

                            switch (chooseDelete)
                            {
                                case 1:

                                    do
                                    {
                                        Console.Write("Введите номер для удаления: ");
                                        result = int.TryParse(Console.ReadLine(), out numberBook);
                                    } while (result == false || numberBook < 1 || numberBook > library.Length);
                                    DeleteBookFromLibraryByNumber(ref library, numberBook);

                                    break;
                                case 2:


                                    Console.Write("Введите название для удаления: ");
                                    nameBook = Console.ReadLine();
                                    DeleteBookFromLibraryByName(ref library, nameBook);

                                    break;
                            }

                        }
                        else
                        {
                            Console.WriteLine("Библиотека пуста");
                            Console.ReadKey();
                        }



                        break;
                    #endregion
                    case 3:
                        #region Изменение

                        if (library.Length > 0)
                        {


                            Console.WriteLine("Выберите способ изменения: ");
                            Console.WriteLine();
                            Console.WriteLine("  1. По номеру: ");
                            Console.WriteLine("  2. По названию: ");

                            do
                            {
                                chooseUpdate = ChoseMenuItem();
                                if (chooseUpdate > 2)
                                {
                                    Console.WriteLine("Неверный ввод, повторите попытку");
                                }

                            } while (chooseUpdate > 2);

                            switch (chooseUpdate)
                            {
                                case 1:

                                    do
                                    {
                                        Console.Write("Введите номер для изменения: ");
                                        result = int.TryParse(Console.ReadLine(), out numberBook);
                                    } while (result == false || numberBook < 1 || numberBook > library.Length);

                                    tempBook = InputNewBook(library);
                                    UpdateBookFromLibraryByNumber(library, numberBook, tempBook);

                                    break;
                                case 2:


                                    Console.Write("Введите название для удаления: ");
                                    nameBook = Console.ReadLine();

                                    tempBook = InputNewBook(library);
                                    UpdateBookFromLibraryByName(library, nameBook, tempBook);

                                    break;
                            }


                        }
                        else
                        {
                            Console.WriteLine("Библиотека пуста");
                            Console.ReadKey();
                        }


                        break;
                    #endregion
                    case 4:
                        #region Сохранение в файл
                        do
                        {
                            Console.Write("Введите имя файла: ");
                            fileName = Console.ReadLine();

                        } while (fileName.Length < 3);

                        SaveLibraryToFile(library, fileName);

                        Console.WriteLine("Файл успешно сохранен.");
                        Console.ReadKey();

                        break;
                    #endregion
                    case 5:
                        #region Для печати
                        do
                        {
                            Console.Write("Введите имя файла: ");
                            fileName = Console.ReadLine();

                        } while (fileName.Length < 3);

                        ExportLibraryToFile(library, fileName);

                        Console.WriteLine("Сформирован файл для печати");
                        Console.ReadKey();

                        break;

                    #endregion
                    case 6:
                        #region Загрузка с файла

                        do
                        {
                            Console.Write("Введите имя файла: ");
                            fileName = Console.ReadLine();

                        } while (fileName.Length < 3);

                        library = LoadLibraryFromRile(fileName);
                        Console.WriteLine("Библиотека успешно загружена");

                        Console.ReadKey();

                        break;

                    #endregion
                    case 0:
                        #region Выход
                        Environment.Exit(0);

                        break;
                        #endregion

                }

            }
        }
    }
}
