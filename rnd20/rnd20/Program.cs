﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rnd20
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int N = 20;
            int[] mas = new int[N];

            for (int i = 0; i < N; i++)
            {
                mas[i] = rnd.Next(0, 100);
                if (i > 0)
                {
                    for (int j = 0; j < i; j++)
                    {
                        if (mas[j] == mas[i])
                        {
                            mas[i] = rnd.Next(0, 100);
                            j = 0;
                        }

                    }

                }

            }

            for (int i = 0; i < N; i++)
            {
                Console.Write(mas[i] + " ");
            }
            Console.WriteLine();



            Console.ReadKey();
        }
    }
}
