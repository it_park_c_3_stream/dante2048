﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sentence
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите текст");
            string text = Console.ReadLine();
            string[] piece = text.Split('.','!','?');
            int IndexMin = 0, IndexMax = 0;
            int MinSize = text.Length, MaxSize = 0;
            string Min = String.Empty, Max = String.Empty;

            for (int i = 0; i < piece.Length; i++) 
            {
              piece[i] = piece[i].Trim(' ');

                if (piece[i].Length > MaxSize)
                {
                    MaxSize = piece[i].Length;
                    IndexMax = i+1;
                    Max = piece[i];

                }
                if (piece[i].Length < MinSize && i < piece.Length - 1)
                {   
                    MinSize = piece[i].Length;
                    IndexMin = i;
                    Min = piece[i];
                }

              Console.WriteLine(piece[i]); 
            }

            Console.WriteLine($"Самое длинное предложение: {Max}");
            Console.WriteLine($"Самое короткое предложение: {Min}");

            Console.ReadKey();



        }
    }
}
