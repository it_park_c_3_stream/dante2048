﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tabmultiplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Таблица умножения");
            Console.WriteLine("");

            int mult = 0;
            string povtor;

        

            try
            {

                do
                {
                    int i = 1;

                    Console.Write("Введите a: ");
                    int a = int.Parse(Console.ReadLine());


                    while (i <= 10)
                    {
                        mult = a * i;
                        Console.WriteLine($"{a}х{i}={mult}");
                        i++;
                    }


                    Console.WriteLine("");
                    Console.Write("Введите 'Y', если хотите повторить: ");
                    povtor = Console.ReadLine();
                }
                while (povtor == "Y");

            }
            catch
            {
                Console.WriteLine("Данные неверны. Программа будет закрыта");
            }

            Console.ReadKey();
        }
    }
}
